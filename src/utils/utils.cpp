#include "utils.hpp"
#include <iostream>

///////  classe utils
Util::Util(float preco, float distancia, int posicao) {
    this->preco = preco;
    this->distancia = distancia;
    this->posicao = posicao;
}

Util::Util() {
    this->preco = 0;
    this->distancia = 0;
    this->posicao = 0;
}

Util::~Util() {

}

bool Util::operator> (Util util) {
    if(this->preco > util.preco) // é maior
        return true;
    if(this->preco < util.preco) // é menor
        return false;
    if(this->distancia > util.distancia) // preco igual mas a disntacia é maior
        return true;
    return false; // preco igual mas a disntacia é menor ou igual
}

bool Util::operator< (Util util) {
    if(this->preco < util.preco) // é menor
        return true;
    if(this->preco > util.preco) // é maior
        return false;
    if(this->distancia < util.distancia) // preco igual mas a disntacia é menor
        return true;
    return false; // preco igual mas a disntacia é maior ou igual
}

void Util::operator= (Util util) {
    this->preco = util.preco;
    this->distancia = util.distancia;
    this->posicao = util.posicao;
}

///// funçoes utils

bool EhFinalDeSemana (string data) {
    string dia = "dd ", mes = "mm ", ano = "aaaa ";

    dia[0] = data.at(0);
    dia[1] = data.at(1);
    dia[2] = '\0';

    mes[0] = data.at(3);
    mes[1] = data.at(4);
    mes[2] = '\0';

    ano[0] = data.at(6);
    ano[1] = data.at(7);
    ano[2] = data.at(8);
    ano[3] = data.at(9);
    ano[4] = '\0';

    int d = atoi(dia.c_str()), m = atoi(mes.c_str()), a = atoi(ano.c_str());

    float k = d + 2 * m + (3 * (m + 1) / 5) + a + a / 4 - a / 100 + a / 400 + 2;

    if((int)k % 7 == 0 || (int)k % 7 == 1) return true;
    return false;
}

void quickSort(Util vetor[ ], int inicio, int fim){
   
   int pivo, i, j, meio;
   Util aux;
   i = inicio;
   j = fim;
   
   meio = (int) ((i + j) / 2);
   pivo = meio;
   
   do{
      while (vetor[i] < vetor[pivo]) i = i + 1;
      while (vetor[j] > vetor[pivo]) j = j - 1;
      
      if(i <= j){
         aux = vetor[i];
         vetor[i] = vetor[j];
         vetor[j] = aux;
         i = i + 1;
         j = j - 1;
      }
   }while(j > i);
   
   if(inicio < j) quickSort(vetor, inicio, j);
   if(i < fim) quickSort(vetor, i, fim);   

}

void decidirMelhorOpcao (PetShop **petShops, string data, int tam, int nGrandes, int nPequenos, float &preco, int &melhor){
    Util vet[tam]; // ATENÇÃO --> ALOCADO ESTATICAMENTE POR CAUDA DE PROBLEMA PARA EXECUTAR COM OS RUNNERS DO GITLAB
                   //     NO TERMINAL DO VSCODE NÃO DÁ PROBLEMAS, MAS NO DO GITLAB TEMOS O ERRO "munmap_chunk(): invalid pointer"

    bool fimDeSemana = EhFinalDeSemana(data);

    if(tam == 0) return;

    for(int i = 0; i < tam; i++) {
        vet[i].preco = petShops[i]->calcularPreco(fimDeSemana, nGrandes, nPequenos);
        vet[i].distancia = petShops[i]->getDistancia();
        vet[i].posicao = i;
    }

    quickSort(vet, 0, tam - 1);

    melhor = vet[0].posicao;
    preco = vet[0].preco;

}