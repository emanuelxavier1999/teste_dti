#ifndef UTILSTEST_HPP
#define UTILSTEST_HPP

#include <assert.h>

#include "../../utils/utils.hpp"

using namespace std;

void operatorMaior ();
void operatorMenor ();
void operatorIgual ();
void EhFimDeSemana ();
void quickSort ();
void decidirMelhorOpcao ();

#endif